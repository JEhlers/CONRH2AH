#include "RH2AH.h"
// Constants
#define AH_CONSTANT 2.16670 // gK/J
#define CRITICAL_TEMPERATURE 647.096 // K
#define CRITICAL_PRESSURE 220640 // hPa
// COEFFICIENTS
#define COEFFICIENT1 -7.85951783
#define COEFFICIENT2 1.84408259
#define COEFFICIENT3 -11.7866497
#define COEFFICIENT4 22.680741
#define COEFFICIENT5 -15.9618719
#define COEFFICIENT6 1.80122502

//  Temperature Conversion
double convertCtoK(double c) {
  return c + 273.15;
}
//  NOT NEEDED
/*double convertKtoC(double k) {
  return k - 273.15;
}
double convertCtoF(double c) {
  return c * 1.8 + 32;
}*/
double convertFtoC(double f) {
  return (f - 32) * 0.55555;
}


// Water vapour saturation pressure in hPa
double calcPws (double temperatureK) {
  const double Theta = 1-(temperatureK/CRITICAL_TEMPERATURE);

  return CRITICAL_PRESSURE * exp(
  (CRITICAL_TEMPERATURE / temperatureK) *
  ( COEFFICIENT1*Theta        + COEFFICIENT2*pow(Theta,1.5) +
    COEFFICIENT3*pow(Theta,3) + COEFFICIENT4*pow(Theta,3.5) +
    COEFFICIENT5*pow(Theta,4) + COEFFICIENT6*pow(Theta,7.5) )
                                );
 }
 
//  Vapour pressure in hPa
double calcPw(double Pws, double relativeHumidity) {
  return Pws * relativeHumidity;
}

//  Absolute humidity in g/m³
double calcAH(double temperatureK, double Pw){
  return AH_CONSTANT * Pw / temperatureK;
}

// Wrapper
double convertRHtoAH (double temperature, double relativeHumidity, Units t) {
  switch (t) {
    case FAHRENHEIT:
    temperature = convertFtoC(temperature);
    case CELSIUS:
    temperature = convertCtoK(temperature);
  }
  return calcAH(temperature,calcPw(calcPws(temperature),relativeHumidity));
}

/*https://www.vaisala.com/sites/default/files/documents/Humidity_Conversion_Formulas_B210973EN-F.pdf*/
