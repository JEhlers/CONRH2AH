#ifndef RH2AH_H_INCLUDED
#define RH2AH_H_INCLUDED

 #include <math.h> /*pow(), exp()*/
 enum Units {CELSIUS, KELVIN, FAHRENHEIT};
 double convertRHtoAH (double temperature, double relativeHumidity, Units t);
 
#endif // RH2AH_H_INCLUDED
